import QtQuick 2.15
import QtQuick.Controls.impl 2.15
import QtQuick.Templates 2.15 as T
import Miko.Asuki 1.0 as Asuki

T.RoundButton {
    id: control

    icon.color: Asuki.TailwindColor.slate[900]

    contentItem: IconLabel {
        spacing: control.spacing
        mirrored: control.mirrored
        display: control.display

        icon: control.icon
        text: control.text
        font: control.font
        color: Asuki.TailwindColor.slate[900]
    }

    background: Rectangle {
        color: control.pressed ? Asuki.TailwindColor.slate[400] :
                                 ((control.hovered || control.focus) && !control.disabled) ? Asuki.TailwindColor.slate[300] : Asuki.TailwindColor.slate[200]
        border.color: control.pressed ? Asuki.TailwindColor.slate[500] :
                                        ((control.hovered || control.focus) && !control.disabled) ? Asuki.TailwindColor.slate[400] : Asuki.TailwindColor.slate[200]
        border.width: 1
        radius: control.radius

        Behavior on color {
            ColorAnimation {
                duration: 100
                easing.type: Easing.InOutCubic
            }
        }

        Behavior on border.color {
            ColorAnimation {
                duration: 100
                easing.type: Easing.InOutCubic
            }
        }
    }
}
