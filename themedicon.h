#ifndef THEMEDICON_H
#define THEMEDICON_H

#include <QtQuick/QQuickPaintedItem>

class ThemedIcon : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    QML_ELEMENT
public:
    ThemedIcon(QQuickItem *parent = nullptr);

    QString name();
    void setName(QString name);

    void paint(QPainter *painter);

signals:
    void nameChanged();

private:
    QString m_name;
};

#endif // THEMEDICON_H
