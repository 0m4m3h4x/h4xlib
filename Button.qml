import QtQuick 2.15
import QtQuick.Controls.impl 2.15
import QtQuick.Templates 2.15 as T
import Miko.Asuki 1.0 as Asuki

T.Button {
    id: control

    icon.color: "transparent"

    contentItem: IconLabel {
        spacing: control.spacing
        mirrored: control.mirrored
        display: control.display

        icon: control.icon
        text: control.text
        font: control.font
        color: Asuki.TailwindColor.slate[900]
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40
        color: control.pressed ? Asuki.TailwindColor.slate[400] :
                                 ((control.hovered || control.focus) && !control.disabled) ? Asuki.TailwindColor.slate[300] : Asuki.TailwindColor.slate[200]
        border.color: control.pressed ? Asuki.TailwindColor.slate[500] :
                                        ((control.hovered || control.focus) && !control.disabled) ? Asuki.TailwindColor.slate[400] : Asuki.TailwindColor.slate[300]
        border.width: 1
        radius: 7

        Behavior on color {
            ColorAnimation {
                duration: 100
                easing.type: Easing.InOutCubic
            }
        }

        Behavior on border.color {
            ColorAnimation {
                duration: 100
                easing.type: Easing.InOutCubic
            }
        }
    }
}
