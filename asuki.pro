TEMPLATE = lib
TARGET = asuki
QT += qml quick dbus KWindowSystem
CONFIG += plugin c++11 qmltypes

QML_IMPORT_NAME = Miko.Asuki
QML_IMPORT_MAJOR_VERSION = 1

DBUS_INTERFACES += settings.xml

TARGET = $$qtLibraryTarget($$TARGET)
uri = Miko.Asuki

# Input
SOURCES += \
        asuki_plugin.cpp \
        blurredwindow.cpp \
        settings.cpp \
        themedicon.cpp

HEADERS += \
        asuki_plugin.h \
        blurredwindow.h \
        settings.h \
        themedicon.h

DISTFILES = qmldir \
    Button.qml \
    Label.qml \
    RoundButton.qml \
    RoundImage.qml \
    TailwindColor.qml \
    IconLabel.qml \
    plugins.qmltypes

qmldir.path = $$[QT_INSTALL_QML]/$$replace(uri, \., /)
qmldir.files = $$DISTFILES

target.path = $$[QT_INSTALL_QML]/$$replace(uri, \., /)

INSTALLS += target qmldir

RESOURCES += \
    resources.qrc
