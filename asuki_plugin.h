#ifndef ASUKI_PLUGIN_H
#define ASUKI_PLUGIN_H

#include <QQmlExtensionPlugin>

class AsukiPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override;
};

#endif // ASUKI_PLUGIN_H
