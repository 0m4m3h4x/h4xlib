#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QtQml>
#include "settings_interface.h"

class Settings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString background READ background WRITE setBackground NOTIFY backgroundChanged)
    Q_PROPERTY(QString accentColor READ accentColor WRITE setAccentColor NOTIFY accentColorChanged)
    Q_PROPERTY(bool darkMode READ darkMode WRITE setDarkMode NOTIFY darkModeChanged)
    QML_ELEMENT

public:
    explicit Settings(QObject *parent = nullptr);

    QString background();
    QString accentColor();
    bool darkMode();

    void setBackground(QString background);
    void setAccentColor(QString accentColor);
    void setDarkMode(bool darkMode);

signals:
    void backgroundChanged();
    void accentColorChanged();
    void darkModeChanged();

public slots:
    void reload();

private:
    Miko::Settings::Background *m_background;
    Miko::Settings::Theme *m_theme;
    Miko::Settings::DaemonSettings *m_daemonSettings;
};

#endif // SETTINGS_H
