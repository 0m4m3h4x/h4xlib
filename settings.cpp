#include "settings.h"

Settings::Settings(QObject *parent)
    : QObject(parent)
{
    m_background = new Miko::Settings::Background("Miko.Settings", "/Background", QDBusConnection::sessionBus(), this);
    m_theme = new Miko::Settings::Theme("Miko.Settings", "/Theme", QDBusConnection::sessionBus(), this);
    m_daemonSettings = new Miko::Settings::DaemonSettings("Miko.Settings", "/Daemon", QDBusConnection::sessionBus(), this);

    connect(m_background, &Miko::Settings::Background::backgroundChanged, this, &Settings::backgroundChanged);
    connect(m_theme, &Miko::Settings::Theme::accentColorChanged, this, &Settings::accentColorChanged);
    connect(m_theme, &Miko::Settings::Theme::darkModeChanged, this, &Settings::darkModeChanged);
}

QString Settings::background() {
    return m_background->background().argumentAt(0).toString();
}

void Settings::setBackground(QString background) {
    m_background->setBackground(background);
}

QString Settings::accentColor() {
    return m_theme->accentColor().argumentAt(0).toString();
}

void Settings::setAccentColor(QString background) {
    m_theme->setAccentColor(background);
}

bool Settings::darkMode() {
    return m_theme->darkMode().argumentAt(0).toBool();
}

void Settings::setDarkMode(bool darkMode) {
    m_theme->setDarkMode(darkMode);
}

void Settings::reload() {
    m_daemonSettings->reload();
}
