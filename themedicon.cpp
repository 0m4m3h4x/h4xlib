#include "themedicon.h"
#include <QIcon>

ThemedIcon::ThemedIcon(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{
}

QString ThemedIcon::name() {
    return m_name;
}

void ThemedIcon::setName(QString name) {
    if (m_name == name) return;
    m_name = name;
    emit nameChanged();
}

void ThemedIcon::paint(QPainter *painter) {
    QIcon icon = QIcon::fromTheme(m_name);
    icon.paint(painter, boundingRect().toRect());
    update();
}
