#include "asuki_plugin.h"
#include "settings.h"
#include "themedicon.h"
#include "blurredwindow.h"
#include <qqml.h>
#include <QFontDatabase>

void AsukiPlugin::registerTypes(const char *uri)
{
    // @uri Miko.Asuki
    qmlRegisterType<Settings>(uri, 1, 0, "Settings");
    qmlRegisterType<ThemedIcon>(uri, 1, 0, "ThemedIcon");
    qmlRegisterType<BlurredWindow>(uri, 1, 0, "BlurredWindow");

    QFontDatabase::addApplicationFont(":/asuki/fonts/MaterialIconsRound-Regular.otf");
}
