import QtQuick 2.15
import QtQuick.Templates 2.15 as T
import Miko.Asuki 1.0 as Asuki

T.Label {
    id: control

    Asuki.Settings {
        id: settings
    }

    color: settings.darkMode ? Asuki.TailwindColor.slate[50] : Asuki.TailwindColor.slate[900]
    font.family: 'Inter'
    font.pixelSize: 16
}
