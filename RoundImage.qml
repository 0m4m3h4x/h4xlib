import QtQuick 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.15

Rectangle {
    id: imageBG

    property alias fillMode: image.fillMode
    property alias source: image.source
    property alias sourceSize: image.sourceSize

    Image {
        id: image
        anchors.fill: parent
        anchors.margins: 1
        mipmap: true
        visible: false

        Rectangle {
            id: imageMask
            visible: false
            anchors.fill: parent
            radius: imageBG.radius - 1
            color: "black"
        }
    }

    OpacityMask {
        source: image
        anchors.fill: image
        maskSource: imageMask
    }
}
