#include "blurredwindow.h"
#include <KWindowEffects>

BlurredWindow::BlurredWindow(QWindow *parent) : QQuickWindow(parent)
{
    this->m_updateRegion();
    connect(this, &QQuickWindow::widthChanged, this, &BlurredWindow::m_updateRegion);
    connect(this, &QQuickWindow::heightChanged, this, &BlurredWindow::m_updateRegion);
    connect(this, &QQuickWindow::visibleChanged, this, &BlurredWindow::m_updateRegion);
    connect(this, &QQuickWindow::activeChanged, this, &BlurredWindow::windowActiveChanged);
}

int BlurredWindow::radius() {
    return m_radius;
}

void BlurredWindow::setRadius(int radius) {
    if (radius == m_radius) return;
    m_radius = radius;
    emit this->radiusChanged();
    this->m_updateRegion();
}

QRegion BlurredWindow::m_getNewRegion() {
    QRegion region;
    QRect rect = QRect(0, 0, this->width(), this->height());

    // middle and edges
    region += rect.adjusted(m_radius, 0, -m_radius, 0);
    region += rect.adjusted(0, m_radius, 0, -m_radius);

    // top left corner
    QRect corner(rect.topLeft(), QSize(m_radius * 2, m_radius * 2));
    region += QRegion(corner, QRegion::Ellipse);

    // top right corner
    corner.moveTopRight(rect.topRight());
    region += QRegion(corner, QRegion::Ellipse);

    // bottom left corner
    corner.moveBottomLeft(rect.bottomLeft());
    region += QRegion(corner, QRegion::Ellipse);

    // bottom right corner
    corner.moveBottomRight(rect.bottomRight());
    region += QRegion(corner, QRegion::Ellipse);

    return region;
}

void BlurredWindow::m_updateRegion() {
    if (!this->isVisible()) return;
    KWindowEffects::enableBlurBehind(this, true, this->m_getNewRegion());
}
