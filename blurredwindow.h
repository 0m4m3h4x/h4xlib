#ifndef BLURREDWINDOW_H
#define BLURREDWINDOW_H

#include <QQuickWindow>

class BlurredWindow : public QQuickWindow
{
    Q_OBJECT
    Q_PROPERTY(int radius READ radius WRITE setRadius NOTIFY radiusChanged)
    Q_PROPERTY(bool active READ isActive NOTIFY activeChanged)
    QML_ELEMENT

public:
    BlurredWindow(QWindow *parent = nullptr);

    int radius();
    void setRadius(int radius);

signals:
    void radiusChanged();
    void windowActiveChanged();

private:
    QRegion m_getNewRegion();
    void m_updateRegion();
    int m_radius = 0;
};

#endif // BLURREDWINDOW_H
